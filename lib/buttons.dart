import 'package:flutter/material.dart';

// creating Stateless Widget for buttons
class MyButton extends StatelessWidget {

  // declaring variables
  final color;
  final textColor;
  final buttonText;
  final buttontapped;

  //Constructor
    MyButton({this.color, this.textColor, this.buttonText, this.buttontapped});

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: buttontapped,
      child: Padding(
        padding: const EdgeInsets.all(0.2),
        child:
        ElevatedButton(
          onPressed: buttontapped,
          child: Text(buttonText),
          style: ElevatedButton.styleFrom(
            shape: CircleBorder(),
            padding: EdgeInsets.all(24),
            primary: color,
            onPrimary: textColor, // Text Color (Foreground color)

          ),
        )
        // ClipRRect(
        //   // borderRadius: BorderRadius.circular(25),
        //   child: Container(
        //     color: color,
        //     child: Center(
        //       child: Text(
        //         buttonText,
        //         style: TextStyle(
        //           color: textColor,
        //           fontSize: 25,
        //           fontWeight: FontWeight.bold,
        //         ),
        //       ),
        //     ),
        //   ),
        // ),
      ),
    );
  }
}