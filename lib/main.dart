import 'package:flutter/material.dart';
import 'package:math_expressions/math_expressions.dart';

import 'buttons.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
          primarySwatch: Colors.blue,
          scaffoldBackgroundColor: const Color(0xFF484343)
      ),
      home: const MyHomePage(title: 'Flutter Demo Home Page'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({super.key, required this.title});

  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {

  var userInput = '';
  var answer = '';

  // Array of button
  final List<String> buttons = [
    'C',
    '()',
    '%',
    '/',
    '7',
    '8',
    '9',
    'x',
    '4',
    '5',
    '6',
    '-',
    '1',
    '2',
    '3',
    '+',
    '+/-',
    '0',
    '.',
    '=',
  ];
  // final List<String> buttons = [
  //   'C',
  //   '+/-',
  //   '%',
  //   'del',
  //   '7',
  //   '8',
  //   '9',
  //   '/',
  //   '4',
  //   '5',
  //   '6',
  //   'x',
  //   '1',
  //   '2',
  //   '3',
  //   '-',
  //   '0',
  //   '.',
  //   '=',
  //   '+',
  // ];

  @override
  Widget build(BuildContext context) {

    return Scaffold(
      backgroundColor: const Color(0xFF000000),
      body: Column(
        children: <Widget>[
          Expanded(
            child: Container(
              child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: <Widget>[
                    Container(
                      padding: EdgeInsets.all(20),
                      alignment: Alignment.centerRight,
                      child: Text(
                        userInput,
                        style: TextStyle(fontSize: 18, color: Colors.white),
                      ),
                    ),
                    Container(
                      padding: EdgeInsets.all(15),
                      alignment: Alignment.centerRight,
                      child: Text(
                        answer,
                        style: TextStyle(
                            fontSize: 30,
                            color: Colors.white,
                            fontWeight: FontWeight.bold),
                      ),
                    )
                  ]),
            ),
          ),
          Expanded(
            flex: 3,
            child: Container(
              child:
              GridView.builder(
                  itemCount: buttons.length,
                  gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                      crossAxisCount: 4),
                  itemBuilder: (BuildContext context, int index) {

                    // Clear Button
                    if (index == 0) {
                      return Container(
                        padding: EdgeInsets.all(10),
                          child:  MyButton(
                            buttontapped: () {
                              setState(() {
                                userInput = '';
                                answer = '0';
                              });
                            },
                            buttonText: buttons[index],
                            color: const Color(0xFF252525),
                            textColor: const Color(0xFFF61111),
                          )
                      );

                    }

                    // +/- button
                    else if (index == 16) {

                      return Container(
                          padding: EdgeInsets.all(10),
                          child:  MyButton(
                            buttontapped: () { },
                            buttonText: buttons[index],
                            color: const Color(0xFF252525),
                            textColor: const Color(0xFFF61111),
                          )
                      );
                    }


                    // () button
                    else if (index == 1) {

                      return Container(
                          padding: EdgeInsets.all(10),
                          child:  MyButton(
                            buttontapped: () { },
                            buttonText: buttons[index],
                            color: const Color(0xFF252525),
                            textColor: const Color(0xFF6CFD00),
                          )
                      );
                    }
                    // % Button
                    else if (index == 2) {

                      return Container(
                          padding: EdgeInsets.all(10),
                          child:  MyButton(
                            buttontapped: () {
                              setState(() {
                                userInput += buttons[index];
                              });
                            },
                            buttonText: buttons[index],
                            color: const Color(0xFF252525),
                            textColor: const Color(0xFF6CFD00),
                          )
                      );
                    }


                    // Delete Button index ke 3
                    // else if (index == 3) {
                    //
                    //   return Container(
                    //       padding: EdgeInsets.all(10),
                    //       child:  MyButton(
                    //         buttontapped: () {
                    //           setState(() {
                    //             userInput =
                    //                 userInput.substring(0, userInput.length - 1);
                    //           });
                    //         },
                    //         buttonText: buttons[index],
                    //         color: Colors.blue[50],
                    //         textColor: Colors.black,
                    //       )
                    //   );
                    //
                    // }

                    // Equal_to Button
                    else if (index == 19) {
                      return Container(
                          padding: EdgeInsets.all(10),
                          child:  MyButton(
                            buttontapped: () {
                              setState(() {
                                equalPressed();
                              });
                            },
                            buttonText: buttons[index],
                            color: const Color(0xFF6FFF00),
                            textColor: Colors.white,
                          )
                      );

                    }

                    //  other buttons
                    else {

                      return Container(
                          padding: EdgeInsets.all(10),
                          child:  MyButton(
                            buttontapped: () {
                              setState(() {
                                userInput += buttons[index];
                              });
                            },
                            buttonText: buttons[index],
                            color: const Color(0xFF252525),
                            textColor: isOperator(buttons[index])
                                ? const Color(0xFF6FFF00)
                                : Colors.white,
                          )
                      );

                    }
                  }), // GridView.builder
            ),
          ),
        ],
      ),
    );
  }
  bool isOperator(String x) {
    if (x == '/' || x == 'x' || x == '-' || x == '+' || x == '=') {
      return true;
    }
    return false;
  }

// function to calculate the input operation
  void equalPressed() {
    String finaluserinput = userInput;
    finaluserinput = userInput.replaceAll('x', '*');

    Parser p = Parser();
    Expression exp = p.parse(finaluserinput);
    ContextModel cm = ContextModel();
    double eval = exp.evaluate(EvaluationType.REAL, cm);
    answer = eval.toString();
  }

}

